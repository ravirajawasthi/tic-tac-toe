import './App.css';
import Board from "../TicTacToeBoard/Board"

function App() {
  return (
    <div id="tic-tac-toe-root">
      <h1 className="text-teal-500 text-center font-bold text-4xl">Tic-Tac-Toe</h1>
      <Board />
    </div>
  );
}

export default App;
