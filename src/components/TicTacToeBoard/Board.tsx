export default function Board() {
  const boxes = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  return (
    <div>
      <h1 className="text-white text-center">Board</h1>
      <div className="w-full py-5">
        <div className="grid grid-cols-3 grid-rows-3 w-80 m-auto h-80">
          {boxes.map((n) => (
            <div className="text-white h-full border border-red-200 flex items-center justify-center w-full">
              {n}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
